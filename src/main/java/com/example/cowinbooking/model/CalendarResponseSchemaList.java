package com.example.cowinbooking.model;

import lombok.Data;

import java.util.List;

@Data
public class CalendarResponseSchemaList {
    List<CalendarResponseSchema> centers;
}
