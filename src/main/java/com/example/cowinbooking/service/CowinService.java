package com.example.cowinbooking.service;

import com.example.cowinbooking.model.CalendarResponseSchema;
import com.example.cowinbooking.model.CalendarResponseSchemaList;
import com.example.cowinbooking.model.Sessions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class CowinService {

    private static Logger logger = LoggerFactory.getLogger(CowinService.class);

    @Autowired
    RestTemplate restTemplate;

    @Value("${districtCodes:395,392}")
    String districtCodes;

    @Value("${cowin.api:https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/}calendarByDistrict?district_id=%s&date=%s")
    String cowinApi;

    @Value("${only18:false}")
    boolean only18;

    @PostConstruct
    @Scheduled(cron = "${frequency:0/20 * * * * *}")
    public void coWinSlotAvailability() {
        logger.warn("----------------------------------------------------------------------------------------");
        String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        HttpEntity<String> request = getStringHttpEntity();

        List<List<List<String>>> availableSlots = Arrays.stream(districtCodes.split(","))
            .parallel()
            .map(districtCode -> {
                var calendarByDistrictUrl = String.format(cowinApi, districtCode, todayDate);
                ResponseEntity<CalendarResponseSchemaList> calendarResponseSchemaList = restTemplate.exchange(calendarByDistrictUrl, HttpMethod.GET, request, CalendarResponseSchemaList.class);

                return calendarResponseSchemaList
                        .getBody()
                        .getCenters()
                        .stream()
                        .map(calendarResponseSchemaListFunction)
                        .collect(Collectors.toList());
            }).collect(Collectors.toList());

        availableSlots.forEach(CowinService::accept);

    }

    private static void stringAccept(String slot) {
        logger.info(slot);
    }

    private static void listAccept(List<String> l2) {
        l2.forEach(CowinService::stringAccept);
    }

    private static void accept(List<List<String>> l1) {
        l1.forEach(CowinService::listAccept);
    }

    Function<CalendarResponseSchema, List<String>> calendarResponseSchemaListFunction = calendarResponseSchema -> {
        List<String> list = new ArrayList<>();
        for (Sessions session : calendarResponseSchema.getSessions()) {
            if (session.getAvailable_capacity_dose1() > 0 && (!only18 || session.getMin_age_limit() == 18)) {
                var slotLoggerLine = String.format("\t\t district_name : %s | center name : %s | vaccine : %s | available_capacity_dose1 : %s | date : %s",
                        calendarResponseSchema.getDistrict_name(),
                        calendarResponseSchema.getName(),
                        session.getVaccine(),
                        session.getAvailable_capacity_dose1(),
                        session.getDate());
                list.add(slotLoggerLine);
            }
        }
        return list;
    };

    private HttpEntity<String> getStringHttpEntity() {
        var headers = new HttpHeaders();
        headers.set("accept", "application/json");
        headers.set("user-agent", "Mozilla/5.0");
        return new HttpEntity<>(headers);
    }
}

@Configuration
class RestTemplateConfig {
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder){
        return  restTemplateBuilder.build();
    }
}
