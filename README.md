# CoWin Slot Availability


## Usage
Import this Maven project and Run Below java main class
```java
com.example.cowinbooking @SpringBootApplication
@EnableScheduling
public class CoWinSlotAvailability
```

## Setup (application.properties configuration)
### Check Slots only for 18+
```bash
only18=true
```
### check every 30 secs
```bash
frequency=0/30 * * * * *
```
### districtCodes=MUMBAI,THANE,RAIGAD
```bash
districtCodes=395,392,393
```

### GET All states
https://cdn-api.co-vin.in/api/v2/admin/location/states -- Maharashtra = 21

### GET District for State
https://cdn-api.co-vin.in/api/v2/admin/location/districts/21 -- MUMBAI = 395